'use strict';

const amqp = require('amqplib/callback_api');
const name = 'amqplib';

function create(url, socketOptions){
	let conn;

	function start(_, callback){
		amqp.connect(url, socketOptions, function (err, _conn) {
			if(err){
				return callback(err);
			}
			conn = _conn;
			return callback();
		});
	}

	function stop(callback){
		if(conn){
			return conn.close(callback);
		}
		return callback();
	}

	function getClient(){
		return conn;
	}

	return Object.freeze({
		start,
		stop,
		name,
		getClient
	});

}
exports = module.exports = create;
