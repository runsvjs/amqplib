'use strict';

const amqp = require('amqplib');
const name = 'amqplib';

function create(url, socketOptions){
	let conn;

	async function start(){
		conn = await amqp.connect(url, socketOptions);
	}

	async function stop(){
		if(conn){
			await conn.close();
		}
	}

	function getClient(){
		return conn;
	}

	return Object.freeze({
		start,
		stop,
		name,
		getClient
	});

}
exports = module.exports = create;
