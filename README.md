# Runsv wrapper for amqplib


[![pipeline status](https://gitlab.com/runsvjs/amqplib/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/amqplib/-/commits/master)
[![coverage report](https://gitlab.com/runsvjs/amqplib/badges/master/coverage.svg)](https://gitlab.com/runsvjs/amqplib/-/commits/master)

## Install

`$ npm install runsv-amqplib`

## Usage
```javascript

// CI_AMQP_URL='amqp://guest:guest@localhost:5672'

const amqplib = callback(CI_AMQP_URL);
const sv = runsv.create();

sv.addService(service);
sv.start(function(err){
const {amqplib} = sv.getClients();
	// Do your magic here...
	amqplib.createChannel(function(err, chan){
		// ...
	});
});
```

### Contributing

## Sample `.env` file (only required if you want to modify this library)
```bash
#.env file sample
CI_AMQP_URL='amqp://guest:guest@localhost:5672'
```

